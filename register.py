#!/usr/bin/env python3

MYACCOUNT = 'foo@bar.example'
MYURL = 'https://mastodon.example/' # Your Mastodon instance

from mastodon import Mastodon
import sys

if len(sys.argv) != 2:
    raise Exception("Usage: %s password" % sys.argv[0])
password = sys.argv[1]

# Register app - only once!
Mastodon.create_app(
     'DNSresolverapp',
     api_base_url = MYURL,
     to_file = 'DNSresolver_clientcred.secret'
)

# Log in
mastodon = Mastodon(
    client_id = 'DNSresolver_clientcred.secret',
    api_base_url = MYRUL
)
mastodon.log_in(
    MYACCOUNT,
    password,
    to_file = 'DNSresolver_usercred.secret'
)



