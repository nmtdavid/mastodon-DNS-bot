#!/usr/bin/env python3

# A Mastodon (decentralized social network) bot to answer to DNS requests.
# Stéphane Bortzmeyer bortzmeyer@afnic.fr
# Official repository at <https://framagit.org/bortzmeyer/mastodon-DNS-bot>

# Defaults that you may change
TIMEOUT = 5 # seconds
NAMESERVER = "::1" # Set it to None to use the default resolver(s)
MYNAME = "DNSresolver"
MYDOMAIN = "mammout.bzh"
LOGGER = MYNAME
BLACKLIST = ["badguy@mastodon.example", "evilinstance.example"] # You can add accounts or entire instances.
# TODO: canonicalize the case?
MAXLOG = 100 # Maximum size of answers to log
DELAY = 0.75 # Sleep time during requests

# Defaults that are better left alone
MASTODON_MAX = 500

# Documentation in <https://mastodonpy.readthedocs.io/en/latest/>
from mastodon import Mastodon, StreamListener

# http://lxml.de/
from lxml import html

# http://www.dnspython.org/
# Documentation in <http://www.dnspython.org/docs/1.15.0/>
from dns import resolver, rdatatype, name, exception, reversename

import re
import logging
import sys
import time
import string
import socket

def is_ip_address(str):
    try:
        addr = socket.inet_pton(socket.AF_INET6, str)
    except socket.error: # not a valid IPv6 address
        try:
            addr = socket.inet_pton(socket.AF_INET, str)
        except socket.error: # not a valid IPv4 address either
            return False
    return True

class myListener(StreamListener):

    def __init__(self):
        self.resolver = resolver.Resolver()
        self.resolver.lifetime = TIMEOUT
        if NAMESERVER is not None:
            self.resolver.nameservers = [NAMESERVER]
        self.log = logging.getLogger("LOGGER")
        self.blacklist = BLACKLIST
        self.blacklist.append("%s@%s" % (MYNAME, MYDOMAIN)) # To avoid loops
        channel = logging.StreamHandler(sys.stdout)
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        channel.setFormatter(formatter)
        self.log.addHandler(channel)
        self.log.setLevel(logging.INFO)
        super()
        
    def on_notification(self, notification):
        # The bot is strictly sequential, handling one notification
        # after the other. Because a DNS request can take a lot of
        # time to process, this is not satisfying. In theory, it would
        # be better to have a parallel bot, using the threading module
        # <https://docs.python.org/3/library/threading.html>. But note
        # it would increase the risk of denial-of-service, in case
        # there are many requests. May be a better rate-limiting would
        # help.
        try:
            qname = None
            qtype = None
            sender = None
            visibility = None
            if notification['type'] == 'mention':
                id = notification['status']['id']
                sender = notification['account']['acct']
                if sender in BLACKLIST:
                    self.log.info("Service refused to %s" % sender)
                    return
                match = re.match("^.*@(.*)$", sender)
                if match:
                    sender_domain = match.group(1)
                    if sender_domain in BLACKLIST:
                        self.log.info("Service refused to %s" % sender)
                        return
                else:
                    # Probably local instance, without a domain name. Note that we cannot blacklist local users.
                    if sender == MYNAME:
                        self.log.info("Loop detected, sender is myself")
                        return
                # TODO Rate-limit per-user?
                visibility = notification['status']['visibility']
                # Mastodon API returns the content of the toot in
                # HTML, just to make our lifes miserable
                doc = html.document_fromstring(notification['status']['content'])
                # Preserve end-of-lines
                # <https://stackoverflow.com/questions/18660382/how-can-i-preserve-br-as-newlines-with-lxml-html-text-content-or-equivalent>
                for br in doc.xpath("*//br"):
                            br.tail = "\n" + br.tail if br.tail else "\n"
                for p in doc.xpath("*//p"):
                            p.tail = "\n" + p.tail if p.tail else "\n"
                body = doc.text_content()
                match = re.match("^@%s\s+([\w\-\.:]+)(\s+([a-zA-Z]+))?(\s*(.*))?$" % MYNAME, body) 
                if match:
                    sname = match.group(1)
                    if not is_ip_address(sname):
                        qname = sname
                    else:
                        qname = reversename.from_address(sname)
                    if match.group(3) is not None and match.group(3) != '':
                        qtype = match.group(3).upper()
                    else:
                        if not is_ip_address(sname):
                            qtype = 'A'
                        else:
                            qtype = 'PTR'
                    expected = True
                    if match.group(5) is not None and match.group(5) != '':
                        answers = "Spurious text at the end: syntax is just domainname plus optional query type"
                    else:
                        try:
                            msg = self.resolver.query(qname, qtype)
                            answers = ""
                            for data in msg:
                                answers = answers + str(data) + "\n"
                        except resolver.NoAnswer:
                            answers = "NODATA (name exists but no data of type %s)" % qtype
                        except resolver.NXDOMAIN:
                            answers = "NXDOMAIN (name does not exist)"
                        except resolver.NoMetaqueries:
                            answers = "ERROR Hey, what did you expect? ANY queries are forbidden"
                        except rdatatype.UnknownRdatatype:
                            answers = "ERROR Unknown query type %s" % qtype
                        except resolver.NoNameservers as error:
                            answers = "ERROR No proper answer (may be a network or DNSSEC issue with %s): %s" % (qname, error) # It would be better to analyze the result,
                            # the generic error message may be too long / detailed
                        except name.LabelTooLong:
                            answers = "ERROR One of the labels of this name is too long"
                        except exception.Timeout: 
                            answers = "ERROR Timeout (may be a network problem with the authoritative name servers)" 
                        except UnicodeError as error:
                            expected = False
                            answers = "ERROR Something wrong with your Unicode string: %s" % error
                        except Exception as error: # TODO may be too revealing on internal
                            # implementation (but this is free software, after all)
                            expected = False
                            answers = "ERROR General failure: %s" % error
                    canswers = answers.replace('@', '(at)') # Rude but
                    # this is to address the case of Mastodon
                    # addresses in a TXT record. May be indicating the
                    # status as a dict with an explicit list of
                    # recipients solves the problem, but I didn't
                    # test.
                    text = "@%s %s" % (sender, canswers)
                    if len(text) >= MASTODON_MAX:
                        text = "@%s Sorry, answer is %d characters, too large for Mastodon" % (sender, len (text))
                    mastodon.status_post(text, in_reply_to_id = id, 
                                         visibility = visibility)
                    lanswers = canswers.replace('\n', ' ')[0:MAXLOG]
                    if expected:
                        self.log.info("%s %s/%s -> %s - %s" % (sender, qname, qtype, lanswers, visibility)) 
                    else:
                        self.log.error("%s %s/%s -> %s - %s" % (sender, qname, qtype, lanswers, visibility))
                else:
                    pass # Produces an error message, at least? Be
                         # careful not to answer if we are just
                         # included in a conversation. Or log it
                         # (dangerous if toot-bombing)?
                time.sleep(DELAY) # Crude rate-limiting 
        except KeyError as error:
            self.log.error("Malformed notification, missing %s" % error)
        except Exception as error: 
            self.log.error("%s %s/%s -> %s" % (sender, qname, qtype, error))
            
mastodon = Mastodon(
    client_id = "%s_clientcred.secret" % MYNAME,
    access_token = "%s_usercred.secret" % MYNAME,
    api_base_url = "https://%s/" % MYDOMAIN)
listener = myListener()
while True:
    try:
        mastodon.stream_user(listener)
    except Exception as error:
        self.log.critical("Unexpected error %s" % (error))
        time.sleep(DELAY)
        
